import Zodiac from './script.js';
import $ from 'jquery';
import './style.css';
import 'bootstrap/dist/css/bootstrap.min.css';
///////////////знак зодиака

$('#buttonZodiac').click(function () {
    if ($('#dateInput').val() === "" ) {
        $('#errorDate').html ("для определения знака необходимо ввести дату");
        $('#errorDate').css("color", "red");

    }else if ((new Date($('#dateInput').val()) > new Date (9999,12,31))){
        $('#errorDate').html ("максимальная дата 31.12.9999");
        $('#errorDate').css("color", "red");
    }else {
        $('#errorDate').html("");
        console.log($('#dateInput').val());
        let obj = new Zodiac();
        let date = $('#dateInput').val();
        $('#zodiac').html (obj.learnSign(date));
    }
});




